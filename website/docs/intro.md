---
sidebar_position: 1
slug: /
title: 'Installation'
---

# Helm Charts

Welcome to the AlphaBravo Helm chart repository.

Below is a list of Helm charts and how to use them.

## Repository

### Add the Repository to Helm
Add the repository.

```bash
helm repo add ablabs https://charts.ablabs.io/
```

### Update the Repository
Update the ABLabs repository:

```bash
helm repo update
```

### Find All Available Charts
Query the repo to find all available charts from AlphaBravo.

```bash
helm search repo ablabs
```

## Installation

### Install or Upgrade ABScan
Generate a values.yaml file.

```bash
helm show values ablabs/abscan > my_values.yaml
```

Install ABScan.

```bash
helm upgrade --install -f my_values.yaml --namespace abscan --create-namespace abscan ablabs/abscan
```

Get Chart and Application version information.

```bash
helm search repo abscan
```

### Install or Upgrade CVE-Search Database Updater
Generate a values.yaml file.

```bash
helm show values ablabs/cve-search > my_values.yaml
```

Install CVE-Search.

```bash
helm upgrade --install -f my_values.yaml --namespace abscan --create-namespace abscan ablabs/cve-search
```

Get Chart and Application version information.

```bash
helm search repo cve-search
```

### Rollback a Deployment

```bash
helm rollback --wait --timeout 20 ablabs/abscan <revisionNumber>
```