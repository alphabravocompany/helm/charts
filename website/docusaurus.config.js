// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: ' ',
  // tagline: 'All Helm charts, all the time.',
  url: 'https://charts.ablabs.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        logo: {
          alt: 'AlphaBravo Logo',
          src: 'img/AB.png',
          srcDark: 'img/AB-dark.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Charts',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'AlphaBravo',
            items: [
              {
                label: 'Homepage',
                href: 'https://alphabravo.io',
              },
              {
                label: 'Github',
                href: 'https://github.com/alphabravocompany/',
              },
              {
                label: 'LinkedIn',
                href: 'https://www.linkedin.com/company/alphabravogov',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Rescue Rover, LLC DBA AlphaBravo.<br>Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
