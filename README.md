# AlphaBravo Helm Charts
This repository is used for publishing the Helm charts for AlphaBravo. It does this by making use of the following technologies:

- Gitlab Pages
- Gitlab CI/CD
- Helm
- Markdown
- Docusaurus

## What's Happening Here?
The Gitlab Pages feature is used to host the documentation for the Helm charts, as well as the Helm charts themselves. The documentation is written in Markdown and converted to a static site using Docusaurus. The Helm charts are pulled from multiple upstream repositories.

## How Does It Work?
Everything is orchestrated using Gitlab CI/CD processes. There are multiple upstream pipelines which can trigger the pipeline in this repository. The upstream repos are:

- [ABScan Helm Chart](https://gitlab.com/alphabravocompany/kubernetes/helmcharts/abscan)
- [CVE-Search Helm Chart](https://gitlab.com/alphabravocompany/kubernetes/helmcharts/cve-search)

The upstream pipeline contains a rule which checks if a tag has been generated and matches a specific regex pattern, `update-0.0.0`. If the tag matches, the upstream pipeline executes and builds its respective Helm chart, and stores the chart artifact for 2 years. Once the artifact has been built successfully, the upstream pipeline triggers the pipeline in this repository.

The pipeline in this repo performs a series of actions to build the documentation and publish the upstream Helm charts. The pipeline performs the following steps:

1. Creates directories to store data.
2. Download and extracts the existing published charts to prevent data loss in a temporary directory.
3. Verifies the directory contents to ensure it's not empty.
4. Downloads the upstream Helm chart artifacts from the `ABScan` and `CVE-Search` repositories.
5. Unzips the artifacts in the same directory structure as the existing published charts.
6. Moves all of the charts, old and new, to the `alphabravo` directory.
7. Creates a new `index.yaml` file to include all of the charts.
8. Uses `yarn` to build the static site via Docusaurus.
9. Moves the `public` directory up one level.
10. Moves the `alphabravo` chart directory and the `index.yaml` file to the `public` directory.

This in turn will create an artifact called `public`. Gitlab Pages will then publish the `public` artifact as a static site.

## Notes of Interest
* The artifacts this repos pipeline pulls from are not dynamic. If a new chart is created, the pipeline will not pull it down until the `.gitlab-ci.yml` file is updated to include the new chart.
* There is some repetition in the `.gitlab-ci.yml` file that can probably be improved. This would also allow for easier addition of new charts.
* Maintenance must occur from time to time with the Docusaurus platform. At the moment, the Docusaurus code exists in the repo statically. This could probably be automated to pull the latest version of Docusaurus from the Internet.

## Slack Notifications
Both the upstream and downstream pipelines contain logic for providing notifications to the AlphaBravo Slack #chatops-pipeline channel. The logic is as follows:

1. If the pipeline fails, a message is sent to the channel with the pipeline, the stage in which if failed, the job ID, and who triggered the pipeline.
2. If the pipeline succeeds, a message is sent to the channel with the pipeline, branch, author, and a URL to the chart.